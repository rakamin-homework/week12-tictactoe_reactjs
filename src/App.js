import * as React from "react";
import {
  Button,
  ChakraProvider,
  VStack,
  Text,
  Flex,
  Box,
} from "@chakra-ui/react";
import { createSlice, configureStore } from "@reduxjs/toolkit";
import { Provider, useDispatch, useSelector } from "react-redux";

const ticTacToe = createSlice({
  name: "ticTacToe",
  initialState: {
    squares: Array(9).fill(null),
    winner: null,
    nextValue: "X",
    status: "X's Turn",
  },
  reducers: {
    selectSquare(state, action) {
      if (!state.winner && !state.squares[action.payload]) {
        const newSquares = [...state.squares];
        newSquares[action.payload] = calculateNextValue(state.squares);
        const winner = calculateWinner(newSquares);
        const nextValue = calculateNextValue(newSquares);
        const status = calculateStatus(winner, newSquares, nextValue);
        return {
          squares: newSquares,
          winner,
          status,
        };
      }
    },
    restartGame() {
      return {
        squares: Array(9).fill(null),
        winner: null,
        nextValue: "X",
        status: "X's Turn",
      };
    },
  },
});

// action
export const { selectSquare, restartGame } = ticTacToe.actions;

// store
const store = configureStore({
  reducer: ticTacToe.reducer,
});

function Board() {
  const { status, squares } = useSelector((state) => state);
  const dispatch = useDispatch();
  function selectSquareHandler(squareIndex) {
    dispatch(selectSquare(squareIndex));
  }

  function restartGameHandler() {
    dispatch(restartGame());
  }

  function renderSquare(i) {
    return (
      <Button
        onClick={() => selectSquareHandler(i)}
        w="100px"
        h="100px"
        variant="solid"
        m="5px"
        colorScheme="linkedin"
        boxShadow="6xl"
        fontSize="4em"
      >
        {squares[i]}
      </Button>
    );
  }

  return (
    <VStack h="80vh" justifyContent="center">
      <Text fontSize="6xl">tic-tac-toe~</Text>
      <Text mt="20px" fontSize="xl">
        {status}
      </Text>
      <Box bgColor="gray.200" p="10px" borderRadius="20" mt="10px">
        <Flex>
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </Flex>
        <Flex>
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </Flex>
        <Flex>
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </Flex>
      </Box>
      <Button
        onClick={restartGameHandler}
        colorScheme="red"
        variant="solid"
        mt="20px"
        shadow="6xl"
      >
        Restart Game
      </Button>
    </VStack>
  );
}

function Game() {
  return (
    <Box
      bgPosition="center"
      bgRepeat="no-repeat"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <div>
        <Board />
      </div>
    </Box>
  );
}

// eslint-disable-next-line no-unused-vars
function calculateStatus(winner, squares, nextValue) {
  return winner
    ? `${winner}: won this game!`
    : squares.every(Boolean)
    ? `Scratch: Cat's game`
    : `${nextValue}'s Turn`;
}

// eslint-disable-next-line no-unused-vars
function calculateNextValue(squares) {
  return squares.filter(Boolean).length % 2 === 0 ? "X" : "O";
}

// eslint-disable-next-line no-unused-vars
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function App() {
  return (
    <ChakraProvider>
      <Provider store={store}>
        <Game />;
      </Provider>
    </ChakraProvider>
  );
}

export default App;
